---
layout: markdown_page
title: "TMRG - Global Voices"
description: "GitLab's Global Voices TMRG"
canonical_path: "/company/culture/inclusion/tmrg-global-voices/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Mission
Increase awareness to GitLab’s globally diverse team, the derivatives of having a global team as it relates to the wider GitLab team as well as the company, and improve the daily work life of our global team members.

## Objectives
The Global Voices TMRG has determined the three main objectives to generate impact on in order to fulfill our mission:
1. Empower global team members to celebrate global representation and its benefits
1. Serve as a springboard for ideas to improve global inclusiveness to the day-to-day of GitLab employees
1. Advocate for global diversity across the organization


## Leads
1. [Eliran Mesika](https://www.gitlab.com/eliran.mesika) - Lead
1. [Marin Jankovski](https://www.gitlab.com/marin) - Co-Lead
1. [Seth Berger](https://www.gitlab.com/sethgitlab) - Co-Lead 


## Executive Sponsors
[Sid Sijbrandij](https://gitlab.com/sytses), CEO.

## How to Participate
* Join the [Google Group](https://groups.google.com/a/gitlab.com/g/globalvoices/)
* Join the [#global-voices-tmrg](https://gitlab.slack.com/archives/C03UHGX4F1P) Slack channel for conversation, announcements, and connecting with other TMRG members. Please introduce yourself and tell us where you are based when you join the channel!
* Open an issue on our GitLab project, https://gitlab.com/gitlab-com/global-voices-tmrg
